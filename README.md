# MindSphere Demo Web Application

> ## Front-End Tree Map

* Src
	* **App.vue**
	* **App.scss**
	* **main.js**
	* **router.js**
	* Assets
		* Images
	* Components
		* **Dashboard.vue**
		* **Factory.vue**
		* Layout
			* **SideBar.vue**
		* Factory
			* **MachineTool.vue**
			* **Printer.vue**
			* **ProductLine.vue**
			* **Report.vue**
			* Report
				* **Information.vue**
				* Charts
					* Failure
						* History.vue
						* Hours.vue
					* Production
						* **common.scss**
						* **MachineTool.vue**
						* **Printer.vue**
						* **ProductLine.vue**
					* Status
						* **common.scss**
						* **MachineTool.vue**
						* **Printer.vue**
						* **ProductLine.vue**
		* Dialog
			* **Maintenance.vue**
			* **Notifaction.vue**
			* **Schedule.vue**
		* Charts
			* Heatmap
				* **MachineTool.vue**
				* **Printer.vue**
				* **ProductLine.vue**
			* Line
				* **MachineTool.vue**
				* **Printer.vue**
				* **ProductLine.vue**
			* MachineStatus
				* **MachineTool.vue**
				* **Printer.vue**
				* **ProductLine.vue**
			* **bar.js**
	* Store (Vuex)
		* **index.js**
		* **event.js**
		* **machine.js**
		* **printer.js**
		* **product.js**
		* **report.js**


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/
config/).


> ## How to get MSP API
> ### First go to [MindSphere.io](https://developer.mindsphere.io/apis/index.html) 
> `MSP API Example`![](./img/MindSphereAPI.png)
> ### Store(Vuex) 
> `src` ->  `store` -> `event.js`
> `Example(Event API)：/api/eventmanagement/v3/events/`![](./img/event.png)

> ## Dashboard Page
> `src` ->  `components` -> `Dashboard.vue`
> ### ![](./img/Dashboard.png)
> ### JavaScript 
> `Data && Conmponents Import`![](./img/DashboardJS.png)

> ## Factory Page
> `src` ->  `components` -> `Factory.vue`
> ### ![](./img/Factory.png)
> ### JavaScript 
> `Data && Conmponents Import`![](./img/FactoryJS.png)

> ## Machine Tool Page
> `src` ->  `components` -> `Factory` -> `MachineTool.vue`
> ### ![](./img/MachineTool.png)
> ### JavaScript 
> `Data && Conmponents Import`![](./img/MachineToolJS.png)

> ## Product Line Page
> `src` ->  `components` -> `Factory` -> `ProductLine.vue`
> ### ![](./img/ProductLine.png)
> ### JavaScript 
> `Data && Conmponents Import`![](./img/ProductLineJS.png)

> ## Printer Page
> `src` ->  `components` -> `Factory` -> `Printer.vue`
> ### ![](./img/Printer.png)
> ### JavaScript 
> `Data && Conmponents Import`![](./img/PrinterJS.png)

> ## Dialog
> ### Notification
> `src` ->  `components` -> `Dialog` -> `Factory.vue`
> ### ![](./img/Notification.png)
> ### JavaScript 
> `Data && Conmponents Import`![](./img/NotificationJS.png)
> ### Schedule
> `src` ->  `components` -> `Dialog` -> `Factory.vue`
> ### ![](./img/Schedule.png)
> ### ![](./img/Schedule-2.png)
> ### JavaScript 
> `Data && Conmponents Import`![](./img/ScheduleJS.png)
> ### Maintenance
> `src` ->  `components` -> `Dialog` -> `Factory.vue`
> ### ![](./img/Maintenance.png)
> ### JavaScript 
> `Data && Conmponents Import`![](./img/MaintenanceJS.png)
> `Data && Conmponents Import`![](./img/MaintenanceJS-2.png)

> ## Report Page
> `src` ->  `components` -> `Factory` -> `Report.vue`
> ### ![](./img/Report.png)
> ### ![](./img/Report-2.png)
> ### JavaScript 
> `Data && Conmponents Import`![](./img/ReportJS.png)

> 
> 
> 
> ## ApexCharts
> [ApexCharts](https://apexcharts.com/)

> ## Vue ChartJS
> [Vue-Chartjs](https://vue-chartjs.org/guide/)

> `Bar Chart`![](./img/BarChart.png)

> ## Vue SVG Guage
> [Vue-Svg-gauge](https://github.com/hellocomet/vue-svg-gauge)
	