/* eslint-disable */
import axios from 'axios';
var date = new Date();
var initInterval = null;
async function setDate(){
    try{
        date = new Date();
        date.setHours(date.getHours() + 8);
        date = date.toISOString();
    }catch(e){
        console.log(e);
    }
}

export default {
    state: {
        yield: {
            machine: 0,
            product: 0,
            printer: 0
        },
        status: {
            machine: [],
            product: [],
            printer: []
        }
    },
    mutations: {
        async getYield(state){
            try{
                await setDate();
                let base = Number.parseInt(date.slice(11,13));
                state.yield.machine = (1440 * (base/24) + Number.parseInt(date.slice(14,16))) * 70;
                state.yield.product = (1440 * (base/24) + Number.parseInt(date.slice(14,16))) * 120;
            }catch(e){
                console.log(e);
            }
        },
        async getStatus(state){
            try{
                await setDate();
                let base = Number.parseInt(date.slice(11,13));
                let machineYielding = Math.round(base * 0.7 * 10) / 10;
                let machineWaiting = Math.round(base * 0.17 * 10) / 10;
                let machineIdle = Math.round(base * 0.13 * 10) / 10;
                let machineAbnormal = 0;
                let machineDisconnect = 0;
                let productYielding = Math.round(base * 0.6 * 10) / 10;
                let productWaiting = Math.round(base * 0.2 * 10) / 10;
                let productIdle = Math.round(base * 0.1 * 10) / 10;
                let productAbnormal = Math.round(base * 0.1 * 10) / 10;
                let productDisconnect = 0;
                state.status.machine = [machineYielding,machineWaiting,machineIdle,machineAbnormal,machineDisconnect];
                state.status.product = [productYielding,productWaiting,productIdle,productAbnormal,productDisconnect];
                state.status.printer = [0,0,0,4,20];
            }catch(e){
                console.log(e);
            }
        }
    },
    actions:{
        getReport({ commit }){
            if(initInterval == null){
              commit('getYield');
              commit('getStatus');
            }
            clearInterval(initInterval);
            initInterval = setInterval(() => {
              commit('getYield');
              commit('getStatus');
            }, 60 * 1000);
        }
    },
    getters: {
        yield: state => state.yield,
        status: state => state.status
    }
}
