/* eslint-disable */
import axios from 'axios';
var start = new Date();
start.setHours(start.getHours() - 8);
start = start.toISOString().toString().slice(0,13) + ':00:00Z';
var end = new Date().toISOString().toString().slice(0,13) + ':00:00Z';

export default {
    state: {
        printerStatus: [],
        printerSeries: [
            {
                name: 'aCrest',
                data:[]
            },
            {
                name: 'Ambient_Temp',
                data:[]
            },
            {
                name: 'Motor_Temp',
                data:[]
            },
            {
                name: 'Raw_Vibration_Signal',
                data:[]
            },
            {
                name: 'vCrest',
                data:[]
            },
        ]
    },
    mutations: {
        async getPrinterStatus(state){
            try{
                let res = await axios.get(`/api/iottsaggregates/v3/aggregates/ed68e42a946441bc8b2a2a54ebb174b2/aspect_Virtual_Motor_Status?from=${start}&to=${end}&intervalValue=1&intervalUnit=hour&select=Motor_Actual_Speed`);
                res.data.forEach(data => {
                    if(data.Motor_Actual_Speed != undefined)
                        state.productStatus.push(data.Motor_Actual_Speed.average);
                });
                console.log(state.productStatus);
            }catch(e){
                console.log(e);
            }
        },
        async getPrinterData(state){
            try{
                let res = await axios.get(`/api/iottsaggregates/v3/aggregates/ed68e42a946441bc8b2a2a54ebb174b2/aspect_Virtual_Motor_Condition?from=${start}&to=${end}&intervalValue=1&intervalUnit=hour`);
                res.data.forEach(data => {
                    if(data.aCrest != undefined){
                        state.printerSeries[0].data.push(Math.floor(data.aCrest.average * 100) / 100);
                    }
                    if(data.Ambient_Temp != undefined){
                        state.printerSeries[1].data.push(Math.floor(data.Ambient_Temp.average * 100) / 100);
                    }
                    if(data.Motor_Temp != undefined){
                        state.printerSeries[2].data.push(Math.floor(data.Motor_Temp.average * 100) / 100);
                    }
                    if(data.Raw_Vibration_Signal != undefined){
                        state.printerSeries[3].data.push(Math.floor(data.Raw_Vibration_Signal.average * 100) / 100);
                    }
                    if(data.vCrest != undefined){
                        state.printerSeries[4].data.push(Math.floor(data.vCrest.average * 100) / 100);
                    }
                });
                console.log(state.printerSeries);
            }catch(e){
                console.log(e);
            }
        }
    },
    getters: {
        productStatus: state => state.productStatus,
        printerSeries: state => state.printerSeries
    }
}
