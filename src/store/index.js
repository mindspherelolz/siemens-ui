import Vue from 'vue'
import Vuex from 'vuex'
import ProductLine from './product'
import MachineTool from './machine'
import Printer from './printer'
import Event from './event'
import Report from './report'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        ProductLine,
        MachineTool,
        Printer,
        Event,
        Report
    }
});