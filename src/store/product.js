/* eslint-disable */
import axios from 'axios';
var start = new Date();
start.setHours(start.getHours() - 8);
start = start.toISOString().toString().slice(0,13) + ':00:00Z';
var end = new Date().toISOString().toString().slice(0,13) + ':00:00Z';

export default {
    state: {
        productStatus: [],
        productSeries: [
            {
                name: 'aCrest',
                data:[]
            },
            {
                name: 'Ambient_Temp',
                data:[]
            },
            {
                name: 'Motor_Temp',
                data:[]
            },
            {
                name: 'Raw_Vibration_Signal',
                data:[]
            },
            {
                name: 'vCrest',
                data:[]
            },
        ],
        aCrest: 0,
        Ambient_Temp: 0,
        Motor_Temp: 0,
        Raw_Vibration_Signal: 0,
        vCrest: 0
    },
    mutations: {
        async getProductStatus(state){
            try{
                let res = await axios.get(`/api/iottsaggregates/v3/aggregates/ed68e42a946441bc8b2a2a54ebb174b2/aspect_Virtual_Motor_Status?from=${start}&to=${end}&intervalValue=1&intervalUnit=hour&select=Motor_Actual_Speed`);
                res.data.forEach(data => {
                    if(data.Motor_Actual_Speed != undefined)
                        state.productStatus.push(data.Motor_Actual_Speed.average);
                });
                console.log(state.productStatus);
            }catch(e){
                console.log(e);
            }
        },
        async getProductData(state){
            try{
                let res = await axios.get(`/api/iottsaggregates/v3/aggregates/ed68e42a946441bc8b2a2a54ebb174b2/aspect_Virtual_Motor_Condition?from=${start}&to=${end}&intervalValue=1&intervalUnit=hour`);
                state.productSeries[0].data = [];
                state.productSeries[1].data = [];
                state.productSeries[2].data = [];
                state.productSeries[3].data = [];
                state.productSeries[4].data = [];
                res.data.forEach(data => {
                    let time = data.starttime.slice(11,19);
                    if(data.aCrest != undefined){
                        let aCrest = {
                            x: time,
                            y:Math.floor(data.aCrest.average * 100) / 100
                        }
                        state.productSeries[0].data.push(aCrest);
                    }
                    if(data.Ambient_Temp != undefined){
                        let Ambient_Temp = {
                            x: time,
                            y:Math.floor(data.Ambient_Temp.average * 100) / 100
                        }
                        state.productSeries[1].data.push(Ambient_Temp);
                    }
                    if(data.Motor_Temp != undefined){
                        let Motor_Temp = {
                            x: time,
                            y:Math.floor(data.Motor_Temp.average * 100) / 100
                        }
                        state.productSeries[2].data.push(Motor_Temp);
                    }
                    if(data.Raw_Vibration_Signal != undefined){
                        let Raw_Vibration_Signal = {
                            x: time,
                            y:Math.floor(data.Raw_Vibration_Signal.average * 100) / 100
                        }
                        state.productSeries[3].data.push(Raw_Vibration_Signal);
                    }
                    if(data.vCrest != undefined){
                        let vCrest = {
                            x: time,
                            y:Math.floor(data.vCrest.average * 100) / 100
                        }
                        state.productSeries[4].data.push(vCrest);
                    }
                });
                let aCrest = state.productSeries[0].data;
                state.aCrest = aCrest[aCrest.length-1].y;
                let Ambient_Temp = state.productSeries[1].data;
                state.Ambient_Temp = Ambient_Temp[Ambient_Temp.length-1].y;
                let Motor_Temp = state.productSeries[2].data;
                state.Motor_Temp = Motor_Temp[Motor_Temp.length-1].y;
                let Raw_Vibration_Signal = state.productSeries[0].data;
                state.Raw_Vibration_Signal = Raw_Vibration_Signal[Raw_Vibration_Signal.length-1].y;
                let vCrest = state.productSeries[0].data;
                state.vCrest = vCrest[vCrest.length-1].y;
            }catch(e){
                console.log(e);
            }
        }
    },
    getters: {
        productStatus: state => state.productStatus,
        productSeries: state => state.productSeries,
        aCrest: state => state.aCrest,
        Ambient_Temp: state => state.Ambient_Temp,
        Motor_Temp: state => state.Motor_Temp,
        Raw_Vibration_Signal: state => state.Raw_Vibration_Signal,
        vCrest: state => state.vCrest,
    }
}
