/* eslint-disable */
import axios from 'axios';

export default {
  state: {
    events: [],
  },
  mutations: {
    async getEvent(state) {
      try {
        let res = await axios.get(`/api/eventmanagement/v3/events/`);
        state.events = res.data._embedded.events.filter((event) => {
          return event.entityId == 'ed68e42a946441bc8b2a2a54ebb174b2';
        });
      } catch (e) {
        console.log(e);
      }
    },
  },
  getters: {
    events: (state) => state.events,
  },
};
