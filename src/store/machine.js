/* eslint-disable */
import axios from 'axios';
var start = new Date();
start.setHours(start.getHours() - 8);
start = start.toISOString().toString().slice(0,13) + ':00:00Z';
var end = new Date().toISOString().toString().slice(0,13) + ':00:00Z';

export default {
    state: {
        machineStatus: [],
        machineSeries: [
            {
                name: 'aCrest',
                data:[]
            },
            {
                name: 'Ambient_Temp',
                data:[]
            },
            {
                name: 'Motor_Temp',
                data:[]
            },
            {
                name: 'Raw_Vibration_Signal',
                data:[]
            },
            {
                name: 'vCrest',
                data:[]
            },
        ]
    },
    mutations: {
        async getMachineStatus(state){
            try{
                let res = await axios.get(`/api/iottsaggregates/v3/aggregates/ed68e42a946441bc8b2a2a54ebb174b2/aspect_Virtual_Motor_Status?from=${start}&to=${end}&intervalValue=1&intervalUnit=hour&select=Motor_Actual_Speed`);
                res.data.forEach(data => {
                    if(data.Motor_Actual_Speed != undefined)
                        state.machineStatus.push(data.Motor_Actual_Speed.average);
                });
            }catch(e){
                console.log(e);
            }
        },
        async getMachineData(state){
            try{
                let res = await axios.get(`/api/iottsaggregates/v3/aggregates/ed68e42a946441bc8b2a2a54ebb174b2/aspect_Virtual_Motor_Condition?from=${start}&to=${end}&intervalValue=1&intervalUnit=hour`);
                res.data.forEach(data => {
                    if(data.aCrest != undefined){
                        state.machineSeries[0].data.push(Math.floor(data.aCrest.average * 100) / 100);
                    }
                    if(data.Ambient_Temp != undefined){
                        state.machineSeries[1].data.push(Math.floor(data.Ambient_Temp.average * 100) / 100);
                    }
                    if(data.Motor_Temp != undefined){
                        state.machineSeries[2].data.push(Math.floor(data.Motor_Temp.average * 100) / 100);
                    }
                    if(data.Raw_Vibration_Signal != undefined){
                        state.machineSeries[3].data.push(Math.floor(data.Raw_Vibration_Signal.average * 100) / 100);
                    }
                    if(data.vCrest != undefined){
                        state.machineSeries[4].data.push(Math.floor(data.vCrest.average * 100) / 100);
                    }
                });
                console.log(state.machineSeries);
            }catch(e){
                console.log(e);
            }
        }
    },
    getters: {
        machineStatus: state => state.machineStatus,
        machineSeries: state => state.machineSeries
    }
}
