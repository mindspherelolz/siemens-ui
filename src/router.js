/* eslint-disable */
import Vue from 'vue';
import Router from 'vue-router'
import Dashboard from  './components/Dashboard.vue'
import Factory from './components/Factory.vue'
import ProductLine from './components/Factory/ProductLine.vue'
import Printer from './components/Factory/Printer.vue'
import MachineTool from './components/Factory/MachineTool.vue'
import Report from './components/Factory/Report.vue'

Vue.use(Router);

export default new Router ({
    routes: [
        {
            path:'/',
            name:'Dashboard',
            component: Dashboard
        },
        {
            path:'/factory',
            name:'Factory',
            component: Factory,
        },
        {
            path: '/product',
            name: 'ProductLine',
            component: ProductLine
        },
        {
            path: '/printer',
            name: 'Printer',
            component: Printer
        },
        {
            path: '/machine',
            name: 'MachineTool',
            component: MachineTool
        },
        {
            path: '/report',
            name: 'Report',
            component: Report
        }
    ]
});