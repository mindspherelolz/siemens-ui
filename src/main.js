/* eslint-disable */
import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import VueSvgGauge from 'vue-svg-gauge'
import VueApexCharts from 'vue-apexcharts'
import VCalendar from 'v-calendar'
import Moment from 'vue-moment'
Vue.use(VueSvgGauge)
Vue.use(VueApexCharts)
Vue.use(VCalendar)
Vue.use(Moment);
Vue.component('apexchart', VueApexCharts)

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
