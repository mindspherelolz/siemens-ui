window.onload = function(e){
    var leading = document.querySelectorAll("a.leadingRegionToggle, .leadingRegionToggleArea > a");
    var context = document.querySelectorAll("a.contextRegionToggle, .contextRegionToggleArea > a");
    var regionsWrapper = document.querySelector(".appWrapper__regions");

    for (var i=0; i < leading.length; i++) {
        leading[i].addEventListener('click', function(e) {
            regionsWrapper.classList.toggle('leadingRegion-is-expanded');
            for (var j=0; j<leading.length; j++) {
                leading[j].classList.toggle('is-activated');
            }
        });
    }

    // for (var i=0; i < leading.length; i++) {
    //     context[i].addEventListener('click', function(e) {
    //         regionsWrapper.classList.toggle('contextRegion-is-expanded');
    //         for (var j=0; j<context.length; j++) {
    //             context[j].classList.toggle('is-activated');
    //         }
    //     });
    // }

    var switchLayoutType = document.querySelector("#toggleSlidingBehaviour");
    switchLayoutType.addEventListener('click', function() {
        regionsWrapper.classList.toggle('appWrapper__regions--pushLayout');
    });

    var toggleLeading = document.querySelector("#toggleLeadingRegionDemo");
    toggleLeading.addEventListener('click', function() {
        regionsWrapper.classList.toggle('has-leadingRegion');
    });

    // var toggleContext = document.querySelector("#toggleContextRegionDemo");
    // toggleContext.addEventListener('click', function() {
    //     regionsWrapper.classList.toggle('has-contextRegion');
    // });
}